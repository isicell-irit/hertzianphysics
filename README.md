# Hertzian Physics
  
This folder contains the body and plugin classes that could be used to simulate a 3D hertzian physics applied on cells using *MecaCell*.

It was tested on Debian 10.

## Prerequisites

- CGAL 4.13
- work on a 3D *MecaCell* simulation
- have a general body class that will inherit the **BodyHertzianPhysics** class
- have a general plugin class or struct  that will use the **PluginHertzianPhysics** class


## How to use it

- include the folder in your *MecaCell* project
- import the **BodyHertzianPhysics.hpp** file in the file containing your simulation general body class
- make your general body class inherit from the **BodyHertzianPhysics** class
```cpp 
template <class cell_t> class Bodies3D : public BodyHertzianPhysics
```
- in your general body class constructor add the following line :
```cpp 
explicit CellBody(cell_t *, MecaCell::Vec pos = MecaCell::Vec::zero())
: BodyHertzianPhysics(pos)
```
- import the **PluginHertzianPhysics.hpp** file in the file containing your general plugin class or struct
- add a **PluginHertzianPhysics** attribute to your  general plugin class or struct, this class has to be templated by a cell class
```cpp
PluginHertzianPhysics<cell_t> physicsPlugin;
```
- in the onRegister method, add the new attribute
```cpp
template <typename world_t>  
void onRegister(world_t* w){  
    w->registerPlugin(physicsPlugin);  
}
```
Your simulation will now implement cells that are subjected to hertzian physics

## How to calibrate your simulation

### For your cells
If you want the physics to be coherent, you will need to set a few parameters.

#### Radius
If your cells' radius has to change during the simulation, you can modify it by calling the setRadius method of the body class. 
For example within the cell class :
```cpp
this->getBody().setRadius(50.);
```
The default radius at construction is set to **10µm**.
Setting the radius will automatically update the mass knowing the current density.

#### Density
If your cells' density has to change during the simulation, you can modify it by calling the setDensity method of the body class. 
For example within the cell class :
```cpp
this->getBody().setDensity(0.0013);
```
The default density at construction is set to **0.001 ng/µm³** = 1000kg/m³, the density of water.
Setting the density will automatically update the mass knowing the current radius.

#### Overlap
The overlap attribute of the body class represents the rate of interpenetration of a cell and is settable.
For example within the cell class :
```cpp
this->getBody().setTargetOverlap(0.25);
```
The default value at construction is set to **0.2**, it means that the adhesion characteristic of the cell is set so that 20% of its radius interpenetrates its neighbors when the repulsion and adhesion forces are in equilibrium.

### Coherence Coefficient
After each calculation of the forces and new positions of each cells, some cells might have moved too much that their real position is not coherent anymore with their position in the Delaunay triangulation.
This coefficient is used as an indicator to know if the Delaunay has to be recomputed or if a cell has to be replace in the triangulation.
You'll have to access it from the world in your scenario at initialization.
```cpp
w.cellPlugin.physicsPlugin.setCoherenceCoeff(0.25);
```
The default value is set to **0.2**, it means that a cell is considered incoherent in the triangulation if its real position its position in the triangulation are distant from 0.2 radius away.
It prevents from useless calculation of the triangulation.

### Spatial Borders

You can limit your space in a block of predifined dimensions.
You'll have to access it from the world in your scenario at initialization.
```cpp
w.cellPlugin.physicsPlugin.setMinConstraint(MecaCell::Vec(-10.,-10.,-10.));
w.cellPlugin.physicsPlugin.setMaxConstraint(MecaCell::Vec(10.,10.,10.));
```
By doing this, you prevent the possible positions of the cell to go beyond a cube. Each cell which x, y or z coordinate is out of range [-10.,10.] is replaced at the nearest border of the cube. 

## Other features

The **PluginHertzianPhysics** class disposes of methods to set physical 3D constraints to prevent your cells to reach unwanted positions.

### Adding a solid plane

You can add invisible infinite walls so that your cells can't go beyond predifined positions. You'll have to access it from the world in your scenario at initialization.
```cpp
w.cellPlugin.physicsPlugin.addPlane(MecaCell::Vec(0, 1, 0), MecaCell::Vec(0, 0, 0), true);
```
The first vector corresponds to the normal vector to the plane, the second one corresponds to a point of the plane and the boolean decides wheter the cells are supposed to adhere to the plane or not.

### Adding symmetrical axes forces

You can add axes forces to obtain a specific constrained shape. You'll have to access it from the world in your scenario at initialization.
```cpp
double coneHeight = 42.;
auto radiusFunc = [=](double axisPos, MecaCell::Vec origin, MecaCell::Vec axis){
		// returns the target radius
		return coneHeight*axisPos;
};
w.cellPlugin.physicsPlugin.addSymAxis(radiusFunc, MecaCell::Vec(0., 1., 0.), MecaCell::Vec(0., 0., 0.), coneHeight, 100., true, 0.001);
```
This will subject all the cells at a maximum distance of 100µm of the y-axis to forces that will shape an empty pointing down cone of height 42 µm. The first vector corresponds ton the direction of the axis, the second to its origin, the next argument is the length of the axis, 100. corresponds to the maximal distance the axis forces are applied, the boolean decides whether the constrained structure is supposed to be empty or filled. The last argument represents the force of attraction to the structure.

### Adding global forces

You can subject all of your cells to the same and permanent forces. That allows, for example, to easily add gravity. You'll have to access it from the world in your scenario at initialization.
```cpp
w.cellPlugin.physicsPlugin.addVelocity(MecaCell::Vec(0, -1, 0));
```
This will set up a force applied to all the cell pointing down the y-axis.

### Simulating a curved box

You can subject your cells to centripetal forces so that they tend to being drawn to the center. You'll have to access it from the world in your scenario at initialization. 
```cpp
w.cellPlugin.physicsPlugin.setCurvedBox(100.,50.,10.);
```
This will subject your cells to centripetal forces when their distance to the center is higher than 100 µm. The forces get higherproportionnaly to the squared of the distance so that a cell moving at 10 µm/s can't go farer than 150µm from the center.
