#ifndef PLUGINBODYHERTZIANPHYSICS_HPP
#define PLUGINBODYHERTZIANPHYSICS_HPP

/**
 * @file PluginHertzianPhysics.hpp
 * @brief Defines the PluginHertzianPhysics class for Delaunay triangulation and Hertzian physics.
 *
 * Contains the plugin class which works with the HertzianPhysics body.
 */

#include <vector>
#include <mecacell/mecacell.h>
#include <math.h>
#include <chrono>
#include "Tensor.hpp"
#include "PhysicsConstraints.hpp"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>

using K = CGAL::Exact_predicates_inexact_constructions_kernel;
using Vb = CGAL::Triangulation_vertex_base_with_info_3<void *, K>;
using Tds = CGAL::Triangulation_data_structure_3<Vb>;
using Delaunay = CGAL::Delaunay_triangulation_3<K, Tds>;
using Point = Delaunay::Point;

/**
 * @namespace HertzianPhysics
 * @brief Namespace for Hertzian physics-related classes and functions.
 */
namespace HertzianPhysics {

    /**
     * @class PluginHertzianPhysics
     * @brief Class for managing Delaunay triangulation and Hertzian physics.
     * 
     * @tparam cell_t Type of the cell.
     */
    template<typename cell_t>
    class PluginHertzianPhysics {

    private:
        Delaunay triangulation;  /**< Contains and computes the Delaunay triangulation of the cells */
        double coherenceCoeff = 0.2;  /**< Coefficient used to determine the spatial coherence of each cell */
        bool areForcesNegligible = false; /**< Flag indicating if forces are negligible */
        bool areMovementsNegligible = false; /**< Flag indicating if movements are negligible */

        MecaCell::Vec minConstraint; /**< Minimum constraint vector */
        MecaCell::Vec maxConstraint; /**< Maximum constraint vector */

        PhysicsConstraints constraints; /**< Object managing physical constraints */
        
        /**
         * @brief Checks the coherence of the Delaunay triangulation.
         * @return False if the triangulation needs to be recomputed.
         */
        bool isDelaunayCoherent() {
            bool coherence = true;

            if (triangulation.number_of_vertices() > 0) {

                typename Delaunay::Finite_vertices_iterator vit;

                double tolerance;
                for (vit = triangulation.finite_vertices_begin();
                     vit != triangulation.finite_vertices_end(); ++vit) { // For each vertex
                    cell_t *c = static_cast<cell_t *>(vit->info());
                    tolerance = c->getBody().getBoundingBoxRadius() * coherenceCoeff;
                    tolerance *= tolerance;
                    if ((c->getBody().getPosition().x() * vit->point().x() +
                        c->getBody().getPosition().y() * vit->point().y() +
                        c->getBody().getPosition().z() * vit->point().z())
                        >= tolerance) {  // Verify the coherence between the real position of a cell and its position in the triangulation
                        coherence = false;
                        break;
                    }
                }
            } else coherence = false;
            return coherence;
        }

        /**
         * @brief Moves the points in the triangulation to match their real positions.
         */
        void moveDelaunay() {
            typename Delaunay::Finite_vertices_iterator vit;
            vit = triangulation.finite_vertices_begin();
            double tolerance;
            for (int i = 0; i < triangulation.number_of_vertices(); ++i) { // For each vertex
                cell_t *c = static_cast<cell_t *>(vit->info());
                tolerance = c->getBody().getBoundingBoxRadius() * coherenceCoeff;
                tolerance *= tolerance;
                if ((c->getBody().getPosition().x() * vit->point().x() +
                    c->getBody().getPosition().y() * vit->point().y() +
                    c->getBody().getPosition().z() * vit->point().z())
                    >= tolerance) { // Verify the coherence between the real position of a cell and its position in the triangulation
                    Point p(c->getBody().getPosition().x(), c->getBody().getPosition().y(),
                            c->getBody().getPosition().z());
                    triangulation.move(vit, p);
                }
                ++vit;
            }
        }

        /**
         * @brief Computes the Delaunay triangulation from the list of cells.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void computeDelaunay(world_t *w) {
            for (cell_t *c : w->cells) c->clearConnectedCells();
            if (!isDelaunayCoherent()) {
                std::vector<Point> points;
                points.reserve(w->cells.size());
                for (cell_t *c : w->cells) points.push_back(Point(c->getBody().getPosition().x(), c->getBody().getPosition().y(), c->getBody().getPosition().z()));

                triangulation.clear();
                triangulation = Delaunay(boost::make_zip_iterator(boost::make_tuple(points.begin(), w->cells.begin())),
                                         boost::make_zip_iterator(boost::make_tuple(points.end(), w->cells.end())));
            }
        }

        /**
         * @brief Adds new cells to the triangulation.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void addCells(world_t *w) {
            std::vector<Point> points;
            points.reserve(w->newCells.size());
            for (cell_t *c : w->newCells) points.push_back(Point(c->getBody().getPosition().x(), c->getBody().getPosition().y(), c->getBody().getPosition().z()));

            triangulation.insert(boost::make_zip_iterator(boost::make_tuple(points.begin(), w->newCells.begin())),
                                 boost::make_zip_iterator(boost::make_tuple(points.end(), w->newCells.end())));
        }

        /**
         * @brief Removes dead cells from the triangulation.
         */
        void removeCells() {
            typename Delaunay::Finite_vertices_iterator vit;

            for (vit = triangulation.finite_vertices_begin();
                 vit != triangulation.finite_vertices_end(); ++vit) { // For each vertex
                auto *c1 = static_cast<cell_t *>(vit->info());

                if (c1->isDead()) {
                    triangulation.remove(vit);
                }
            }
        }

        /**
         * @brief Updates the connections between cells.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void updateConnections(world_t *w) {
            if (!triangulation.is_valid()) {
                std::cout << "UNVALID DELAUNAY !!" << std::endl;
            }
            for (auto c : w->cells) c->clearConnectedCells();

            typename Delaunay::Finite_edges_iterator eit;

            for (eit = triangulation.finite_edges_begin();
                 eit != triangulation.finite_edges_end(); ++eit) { // For each connection
                cell_t *c1 = static_cast<cell_t *>(eit->first->vertex(eit->second)->info());
                cell_t *c2 = static_cast<cell_t *>(eit->first->vertex(eit->third)->info());
                if (c1 != nullptr && c2 != nullptr) {
                    double maxdist = c1->getBody().getBoundingBoxRadius() + c2->getBody().getBoundingBoxRadius();
                    if ((c1->getBody().getPosition() - c2->getBody().getPosition()).sqlength() <=
                        maxdist * maxdist) { // Max distance to consider 2 neighbour cells
                        c1->addConnectedCell(c2);
                        c2->addConnectedCell(c1);
                    }
                } else {
                    exit(41);
                }
            }
        }

        /**
         * @brief Applies custom spatial constraints to the cells.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void customConstraint(world_t *w) {
            for (auto c : w->cells) {
                double minConstX = minConstraint.x() + c->getBody().getBoundingBoxRadius();
                double minConstY = minConstraint.y() + c->getBody().getBoundingBoxRadius();
                double minConstZ = minConstraint.z() + c->getBody().getBoundingBoxRadius();
                
                double maxConstX = maxConstraint.x() - c->getBody().getBoundingBoxRadius();
                double maxConstZ = maxConstraint.z() - c->getBody().getBoundingBoxRadius();
                double maxConstY = maxConstraint.y() - c->getBody().getBoundingBoxRadius();

                if (c->getBody().getPosition().x() < minConstX) {
                    c->getBody().setPosition(
                            MecaCell::Vec(minConstX, c->getBody().getPosition().y(), c->getBody().getPosition().z()));
                } else if (c->getBody().getPosition().x() > maxConstX) {
                    c->getBody().setPosition(
                            MecaCell::Vec(maxConstX, c->getBody().getPosition().y(), c->getBody().getPosition().z()));
                }
                if (c->getBody().getPosition().y() < minConstY) {
                    c->getBody().setPosition(
                            MecaCell::Vec(c->getBody().getPosition().x(), minConstY, c->getBody().getPosition().z()));
                } else if (c->getBody().getPosition().y() > maxConstY) {
                    c->getBody().setPosition(
                            MecaCell::Vec(c->getBody().getPosition().x(), maxConstY, c->getBody().getPosition().z()));
                }
                if (c->getBody().getPosition().z() < minConstZ) {
                    c->getBody().setPosition(
                            MecaCell::Vec(c->getBody().getPosition().x(), c->getBody().getPosition().y(), minConstZ));
                } else if (c->getBody().getPosition().z() > maxConstZ) {
                    c->getBody().setPosition(
                            MecaCell::Vec(c->getBody().getPosition().x(), c->getBody().getPosition().y(), maxConstZ));
                }
            }
        }

        /**
         * @brief Updates the forces acting on the cells.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         * @return Minimum time step for the simulation.
         */
        template<typename world_t>
        double updateForces(world_t *w) {
            double dtMin2 = w->dt * w->dt;

            double currentDt2 = dtMin2; // s²

            for (auto c : w->cells) {
                updateInteractionForces(c);
                c->getBody().receiveForce(c->getBody().getPurposeVelocity() * c->getBody().getGamma());
                c->getBody().receiveForce(constraints.getConstraintVelocity(c) * c->getBody().getGamma());
            }

            for (auto c : w->cells) {
                c->getBody().setComputed(false);
                MecaCell::Vector3D vel = c->getBody().getForce() / c->getBody().getGamma();
                c->getBody().setVelocity(vel);
                currentDt2 = std::pow(coherenceCoeff * c->getBody().getBoundingBoxRadius(), 2) / vel.sqlength();
                if (currentDt2 < dtMin2) dtMin2 = currentDt2;
            }
            return sqrt(dtMin2);
        }

        /**
         * @brief Updates the interaction forces between cells.
         * 
         * @param c1 Pointer to the first cell.
         */
        void updateInteractionForces(cell_t *c1) {
            double delta, R, E, Frep, Fadh;
            areForcesNegligible = true;
            for (auto c2 : c1->getConnectedCells()) {
                if (c2 != nullptr && !c2->getBody().getComputed()) {
                    MecaCell::Vec rij(c2->getBody().getPosition() - c1->getBody().getPosition());

                    delta = c1->getBody().getBoundingBoxRadius() + c2->getBody().getBoundingBoxRadius() - rij.length();
                    if (delta > 0) {
                        R = 1.0 / (1.0 / c1->getBody().getBoundingBoxRadius() + 1.0 / c2->getBody().getBoundingBoxRadius());
                        E = 1.0 /
                            ((1.0 - c1->getBody().getPoissonNumber() * c1->getBody().getPoissonNumber()) / c1->getBody().getYoungModulus() +
                            (1.0 - c2->getBody().getPoissonNumber() * c2->getBody().getPoissonNumber()) / c2->getBody().getYoungModulus());

                        Frep = 4.0 / 3.0 * E * sqrt(R) * pow(delta, 3.0 / 2.0);
                        Fadh = -M_PI * R *
                                      (c1->getBody().getAdhesion() + c2->getBody().getAdhesion()) / 2.0 *
                                      M_PI * delta * R;
                        MecaCell::Vec direction = rij.normalized();
                        c1->getBody().receiveForce(-direction * (Frep + Fadh));
                        c2->getBody().receiveForce(direction * (Frep + Fadh));
                        c1->getBody().addStress(Tensor(-direction * (Frep + Fadh), rij));
                        c2->getBody().addStress(Tensor(direction * (Frep + Fadh), -rij));
                    }
                }
            }
            c1->getBody().setComputed(true);
            c1->getBody().divTensor(c1->getBody().getVolume());
        }

        /**
         * @brief Updates the positions of the cells.
         *
         * Updates positions and checks if the movements are too weak to be considered.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         * @param dt Time step.
         */
        template<typename world_t>
        void updatePositions(world_t *w, double dt) {
            double radius;
            areMovementsNegligible = true;
            for (cell_t *c : w->cells) {
                MecaCell::Vec vel = c->getBody().getVelocity();
                radius = c->getBody().getBoundingBoxRadius();

                c->getBody().setPosition(c->getBody().getPosition() + vel * dt);
                if (areMovementsNegligible && vel.sqlength() > coherenceCoeff * coherenceCoeff * radius * radius)
                    areMovementsNegligible = false;
                
                constraints.snapCellsToPlane(c);
            }
        }

        /**
         * @brief Resets the forces acting on the cells.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        inline void resetForces(world_t *w) {
            for (cell_t *c : w->cells) {
                c->getBody().resetForce();
            }
        }

        /**
         * @brief Applies the physics with a coherent time step.
         *
         * Applies the physics with a coherent physics dt.
         * Stops when movements or forces are too weak.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void applyPhysics(world_t *w) {
            double time = 0.0;
            double dt;
            int nbStep = 0;

            do {
                resetForces(w);
                dt = updateForces(w);
                if (time + dt > w->dt) dt = w->dt - time;
                updatePositions(w, dt);
                time += dt;

                nbStep++;
            } while (time < w->dt);
        }

    public:
        
        /**
         * @brief Constructor.
         *
         * Initializes the custom constraints.
         */
        inline PluginHertzianPhysics() {
            minConstraint = MecaCell::Vec(-99999.0, -99999.0, -99999.0);
            maxConstraint = MecaCell::Vec(99999.0, 99999.0, 99999.0);
        }

        /**
         * @brief Sets the coherence coefficient.
         * @param c Coherence coefficient.
         */
        inline void setCoherenceCoeff(double c) { coherenceCoeff = c; }

        /**
         * @brief Begin update hook for MecaCell.
         *
         * Recomputes or readjusts the triangulation and regenerates the connections.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void beginUpdate(world_t *w) {
            computeDelaunay(w);
            updateConnections(w);
        }

        /**
         * @brief On add cell hook for MecaCell.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template <typename world_t>
        void onAddCell(world_t *w) {
            addCells(w);
        }

        /**
         * @brief Pre-delete dead cells update hook for MecaCell.
         *
         * Removes the dead cells from the triangulation.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void preDeleteDeadCellsUpdate(world_t *w) {
            for (cell_t *c : w->cells) {
                if (c->isDead()) {
                    removeCells();
                    break;
                }
            }
        }

        /**
         * @brief Pre-behavior update hook for MecaCell.
         *
         * Applies the physics to each cell.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void preBehaviorUpdate(world_t *w) {
            applyPhysics(w);
        }

        /**
         * @brief Sets the minimum constraint.
         * 
         * @param min Minimum constraint vector.
         */
        void setMinConstraint(MecaCell::Vec min) {
            minConstraint = min;
        }

        /**
         * @brief Sets the maximum constraint.
         * 
         * @param max Maximum constraint vector.
         */
        void setMaxConstraint(MecaCell::Vec max) {
            maxConstraint = max;
        }

        /**
         * @brief Gets the physics constraints.
         * @return PhysicsConstraints object.
         */
        inline PhysicsConstraints getConstraints() { return constraints; }

        /**
         * @brief Adds a plane constraint.
         * 
         * @param _n Normal vector of the plane.
         * @param _p Point on the plane.
         * @param _sticky Indicates if the plane is sticky.
         */
        inline void addPlane(MecaCell::Vec _n = MecaCell::Vec(0, 1, 0), MecaCell::Vec _p = MecaCell::Vec(0, 0, 0), bool _sticky = false) { constraints.addPlane(_n, _p, _sticky); }

        /**
         * @brief Adds a symmetrical axis constraint.
         * 
         * @param _radiusFunction Radius function.
         * @param _axis Axis vector.
         * @param _origin Origin point.
         * @param _length Length of the axis.
         * @param _maxEffectRadius Maximum effect radius.
         * @param _fitShape Indicates if the shape should fit.
         * @param _velocity_attraction Attraction velocity.
         */
        inline void addSymAxis(std::function<double(double, MecaCell::Vec, MecaCell::Vec)> _radiusFunction, MecaCell::Vec _axis = MecaCell::Vec(0., 1., 0.), MecaCell::Vec _origin = MecaCell::Vec(0., 0., 0.), double _length = 100, double _maxEffectRadius = 100, bool _fitShape = true, double _velocity_attraction = 0.001) {
            constraints.addSymAxis(_radiusFunction, _axis, _origin, _length, _maxEffectRadius, _fitShape, _velocity_attraction);
        }

        /**
         * @brief Adds a velocity constraint.
         * 
         * @param _v Velocity vector.
         */
        inline void addVelocity(MecaCell::Vec _v = MecaCell::Vec(0, 1, 0)) { constraints.addVelocity(_v); }

        /**
         * @brief Sets the curved box parameters.
         * 
         * @param radiusBoxPlane Radius of the box plane.
         * @param curvedDistToMaxSpeed Curved distance to maximum speed.
         * @param maxSpeed Maximum speed.
         */
        inline void setCurvedBox(double radiusBoxPlane, double curvedDistToMaxSpeed, double maxSpeed) { constraints.setCurvedBox(radiusBoxPlane, curvedDistToMaxSpeed, maxSpeed); }

    };
}
#endif
