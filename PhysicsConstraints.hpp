#ifndef PHYSICSCONSTRAINTS_HPP
#define PHYSICSCONSTRAINTS_HPP

#include <vector>
#include <math.h>
#include <mecacell/mecacell.h>
#include <functional>

/**
 * @class PhysicsConstraints
 * @brief Class for managing physical constraints in a simulation.
 */
class PhysicsConstraints {
private:
    /**
     * @class Plane
     * @brief Represents a plane constraint.
     */
    class Plane {
    public:
        MecaCell::Vec n; /**< Normal vector of the plane */
        MecaCell::Vec p; /**< Point on the plane */
        bool sticky; /**< Indicates if the plane is sticky */

        /**
         * @brief Constructor for Plane.
         * 
         * @param _n Normal vector of the plane.
         * @param _p Point on the plane.
         * @param _sticky Indicates if the plane is sticky.
         */
        Plane(MecaCell::Vec _n = MecaCell::Vec(0, 1, 0), MecaCell::Vec _p = MecaCell::Vec(0, 0, 0), bool _sticky = false) {
            n = _n;
            n.normalize();
            p = _p;
            sticky = _sticky;
        }

        /**
         * @brief Computes the distance from a point to the plane.
         * 
         * @param c Point to compute the distance from.
         * @return Distance to the plane.
         */
        double distanceToPoint(MecaCell::Vec c) {
            if (n.x() * c.x() + n.y() * c.y() + n.z() * c.z() + (-n.x() * p.x() - n.y() * p.y() - n.z() * p.z()) < 0)
                return -(MecaCell::Vec::getProjectionOnPlane(p, n, c) - c).length();
            else
                return (MecaCell::Vec::getProjectionOnPlane(p, n, c) - c).length();
        }
    };
    std::vector<Plane> planes;

    /**
     * @class SymAxis
     * @brief Represents a symmetrical axis constraint.
     */
    class SymAxis {
    public:
        MecaCell::Vec axis; /**< Axis vector */
        MecaCell::Vec origin; /**< Origin point */
        double length; /**< Length of the axis */
        double maxEffectRadius; /**< Maximum effect radius */
        bool fitShape; /**< Indicates if the shape should fit */
        double attraction_velocity; /**< Attraction velocity */
        std::function<double(double, MecaCell::Vec, MecaCell::Vec)> radiusFunction; /**< Radius function */

        /**
         * @brief Constructor for SymAxis.
         * 
         * @param a Axis vector.
         * @param o Origin point.
         * @param l Length of the axis.
         * @param mef Maximum effect radius.
         * @param fs Indicates if the shape should fit.
         * @param av Attraction velocity.
         * @param rf Radius function.
         */
        SymAxis(MecaCell::Vec a, MecaCell::Vec o, double l, double mef, bool fs, double av, std::function<double(double, MecaCell::Vec, MecaCell::Vec)> rf)
            : axis(a), origin(o), length(l), maxEffectRadius(mef), fitShape(fs), attraction_velocity(av), radiusFunction(rf) {}
    };
    std::vector<SymAxis> symAxes;

    std::vector<MecaCell::Vec> velocities;
    double ratioPoisson2OnYoung = 0.0000000000001;
    double velocity_center_attraction = 0.00001;
    double radiusBox = -1;

public:
    double overlapAdhesion = 0.2;

    /**
     * @brief Default constructor.
     */
    PhysicsConstraints() {}

    /**
     * @brief Adds a plane constraint.
     * 
     * @param _n Normal vector of the plane.
     * @param _p Point on the plane.
     * @param _sticky Indicates if the plane is sticky.
     */
    void addPlane(MecaCell::Vec _n = MecaCell::Vec(0, 1, 0), MecaCell::Vec _p = MecaCell::Vec(0, 0, 0), bool _sticky = false) {
        planes.push_back(Plane(_n, _p, _sticky));
    }

    /**
     * @brief Adds a symmetrical axis constraint.
     * 
     * @param _radiusFunction Radius function.
     * @param _axis Axis vector.
     * @param _origin Origin point.
     * @param _length Length of the axis.
     * @param _maxEffectRadius Maximum effect radius.
     * @param _fitShape Indicates if the shape should fit.
     * @param _velocity_attraction Attraction velocity.
     */
    void addSymAxis(std::function<double(double, MecaCell::Vec, MecaCell::Vec)> _radiusFunction, MecaCell::Vec _axis = MecaCell::Vec(0., 1., 0.), MecaCell::Vec _origin = MecaCell::Vec(0., 0., 0.), double _length = 100, double _maxEffectRadius = 100, bool _fitShape = true, double _velocity_attraction = 0.001) {
        symAxes.push_back(SymAxis(_axis, _origin, _length, _maxEffectRadius, _fitShape, _velocity_attraction, _radiusFunction));
    }

    /**
     * @brief Adds a velocity constraint.
     * 
     * @param _v Velocity vector.
     */
    void addVelocity(MecaCell::Vec _v = MecaCell::Vec(0, 1, 0)) {
        velocities.push_back(_v);
    }

    /**
     * @brief Computes the symmetrical axis constraints velocity for a cell.
     * 
     * @tparam cell_t Type of the cell.
     * @param c Pointer to the cell.
     * @return Symmetrical axis constraints velocity.
     */
    template<typename cell_t>
    MecaCell::Vec getSymAxesConstraintsVelocity(cell_t* c) {
        MecaCell::Vec ret = MecaCell::Vec::zero();
        for (auto symAxis : symAxes) {
            double distOnAxis = symAxis.axis.normalized().dot((c->getBody().getPosition() - symAxis.origin));
            // proj dist to origin
            if (distOnAxis > 0 && distOnAxis < symAxis.length) {
                // dist to axis
                MecaCell::Vec dir = (c->getBody().getPosition() - (symAxis.axis.normalized() * distOnAxis + symAxis.origin));
                double sqDistToAxis = dir.sqlength();
                if (sqDistToAxis > 0) dir.normalize();

                if (sqDistToAxis < symAxis.maxEffectRadius * symAxis.maxEffectRadius) {
                    double targetRadius = symAxis.radiusFunction(distOnAxis / symAxis.length, symAxis.origin, symAxis.axis.normalized() * symAxis.length);
                    double sqDistToRadius = targetRadius * targetRadius - sqDistToAxis;
                    if ((sqDistToRadius > 0 && symAxis.fitShape) || sqDistToRadius < 0) {
                        // push cell to radius
                        ret = dir * (sqDistToRadius > 0 ? 1 : -1) * symAxis.attraction_velocity;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * @brief Computes the constraint velocity for a cell.
     * 
     * @tparam cell_t Type of the cell.
     * @param c Pointer to the cell.
     * @return Constraint velocity.
     */
    template<typename cell_t>
    MecaCell::Vec getConstraintVelocity(cell_t* c) {
        // force pushing cells to center
        double distToCenter = c->getBody().getPosition().length();
        MecaCell::Vec ret = (radiusBox == -1 || distToCenter <= radiusBox)
            ? MecaCell::Vec::zero()
            : -c->getBody().getPosition().normalized() * std::pow(distToCenter - radiusBox, 2.0) * velocity_center_attraction;
        // constraints velocities
        for (auto v : velocities)
            ret += v;
        // shape constraints
        ret += getSymAxesConstraintsVelocity(c);
        return ret;
    }

    /**
     * @brief Sets the curved box parameters.
     * 
     * @param radiusBoxPlane Radius of the box plane.
     * @param curvedDistToMaxSpeed Curved distance to maximum speed.
     * @param maxSpeed Maximum speed.
     */
    void setCurvedBox(double radiusBoxPlane, double curvedDistToMaxSpeed, double maxSpeed) {
        radiusBox = radiusBoxPlane;
        velocity_center_attraction = maxSpeed / std::pow(curvedDistToMaxSpeed, 2.0);
    }

    /**
     * @brief Sets the Poisson's ratio and Young's modulus for the plane.
     * 
     * @param poisson Poisson's ratio.
     * @param young Young's modulus.
     */
    void setPoissonAndYoungOfPlane(double poisson, double young) {
        ratioPoisson2OnYoung = (1.0 - poisson * poisson) / young;
    }

    /**
     * @brief Applies plane constraints to a cell.
     * 
     * @tparam cell_t Type of the cell.
     * @param c Pointer to the cell.
     */
    template<typename cell_t>
    void applyPlaneConstraints(cell_t* c) {
        double dist, delta, R, E, Frep, adhesion, Fadh;
        for (auto plane : planes) {
            if (plane.sticky) {
                dist = std::abs(plane.distanceToPoint(c->getBody().getPosition()));
                if (dist <= c->getBody().getBoundingBoxRadius()) {
                    delta = c->getBody().getBoundingBoxRadius() - dist;
                    R = 1.0 / (1.0 / c->getBody().getBoundingBoxRadius() + 1.0 / c->getBody().getBoundingBoxRadius());
                    E = 1.0 /
                        ((1.0 - c->getBody().getPoissonNumber() * c->getBody().getPoissonNumber()) / c->getBody().getYoungModulus() +
                        ratioPoisson2OnYoung);

                    Frep = 4.0 / 3.0 * E * sqrt(R) * pow(delta, 3.0 / 2.0);
                    adhesion = (4.0 * sqrt(overlapAdhesion * c->getBody().getBoundingBoxRadius()) * E * sqrt(R)) / (3.0 * M_PI * M_PI * R * R);
                    Fadh = -M_PI * M_PI * R * R * adhesion * delta;

                    c->getBody().receiveForce(plane.n * (Frep + Fadh));
                }
            }
        }
    }

    /**
     * @brief Snaps cells to the plane.
     * 
     * @tparam cell_t Type of the cell.
     * @param c Pointer to the cell.
     */
    template<typename cell_t>
    void snapCellsToPlane(cell_t* c) {
        double dist;
        for (auto plane : planes) {
            dist = plane.distanceToPoint(c->getBody().getPosition()) - (c->getBody().getBoundingBoxRadius() - c->getBody().getBoundingBoxRadius() * overlapAdhesion);
            if (dist < 0) c->getBody().setPosition(c->getBody().getPosition() - plane.n * dist);
        }
    }
};

#endif // PHYSICSCONSTRAINTS_HPP
