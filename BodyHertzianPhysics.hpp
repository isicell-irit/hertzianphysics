#ifndef BODYMASSSPRINGDAMPER_HPP
#define BODYMASSSPRINGDAMPER_HPP
#define MOVABLE

/**
 * @file BodyHertzianPhysics.hpp
 * @brief Defines the BodyHertzianPhysics class for Delaunay triangulation and Hertzian physics.
 *
 * Contains the body class which works with the HertzianPhysics plugin.
 */

#include <mecacell/mecacell.h>
#include <mecacell/movable.h>
#include "PluginHertzianPhysics.hpp"
#include "../../../../src/core/BaseBody.hpp"
#include <math.h>
#include "Tensor.hpp"

/**
 * @namespace HertzianPhysics
 * @brief Namespace for Hertzian physics-related classes and functions.
 */
namespace HertzianPhysics {

    /**
     * @class BodyHertzianPhysics
     * @brief Class for managing Delaunay triangulation and mass-spring-based physics.
     * 
     * @tparam cell_t Type of the cell.
     * @tparam plugin_t Type of the plugin.
     */
    template<typename cell_t, class plugin_t>
    class BodyHertzianPhysics : public MecaCell::Movable, public virtual BaseBody<plugin_t> {

    private:
        double baseRadius; /**< Radius in µm */
        double radius; /**< Radius in µm */
        double volume; /**< Volume in µm³ */
        double density; /**< Density in ng/µm³ */
        double gammaCoef; /**< Damping coefficient in Hz */
        MecaCell::Vec purposeVelocity; /**< Speed vector of a moving cell in µm/s */
        double poissonNumber; /**< Ratio of deformation */
        double youngModulus; /**< Stiffness in ng/(µm.s²) */
        double adhesion; /**< Adhesion coefficient in ng/(µm².s²) */
        double targetOverlap; /**< Targeted overlap ratio between cells (used to compute adhesion) */
        bool computed;
        Tensor stressTensor; /**< Stress tensor of a cell */

        /**
         * @brief Computes the adhesion coefficient based on the different parameters.
         */
        inline void computeAdhesion() {
            double E = 1.0 / (2.0 * (1.0 - poissonNumber * poissonNumber) / youngModulus);
            double R = 1.0 / (2.0 / radius);
            adhesion = (4.0 * sqrt(targetOverlap * radius) * E * sqrt(R)) /
                    (3.0 * M_PI * M_PI * R * R);
        }

    public:
        
        /**
         * @brief Constructor.
         *
         * Initializes the Body to default values.
         */
        BodyHertzianPhysics()
        : stressTensor()
        {
            baseRadius = 10.0; // 10 µm radius
            radius = baseRadius;
            volume = 4.0 * M_PI / 3.0 * pow(radius, 3); // µm³
            density = 0.001; // Density of water ~ 1000 kg/m³ = 10¹⁵ ng/m³ = 10^-3 ng/µm³ 
            poissonNumber = 0.5; // Coefficient between 0 and 1
            youngModulus = 4000000000.0; // 4 kPa = 10³ kg/(m.s²) = 10¹⁵ ng/(m.s²) = 10⁹ ng/(µm.s²) 
            this->setMass(density * volume); // Mass = density * volume µg
            gammaCoef = 100000000000.0;
            purposeVelocity = MecaCell::Vec::zero();
            setTargetOverlap(0.2);
        }

        /**
         * @brief Gets the bounding box radius.
         * @return Radius.
         */
        inline double getBoundingBoxRadius() const { return radius; }

        /**
         * @brief Sets the radius.
         *
         * Changes the mass knowing the current density.
         *
         * @param rad Radius.
         */
        inline void setRadius(double rad) {
            radius = rad;
            volume = 4.0 * M_PI / 3.0 * pow(radius, 3.0);
            this->setMass(density * volume); // Mass = density * 4π/3 * radius³ ng
            computeAdhesion();
        }

        /**
         * @brief Gets the volume.
         * @return Volume.
         */
        inline double getVolume() const { return volume; }

        /**
         * @brief Gets the radius.
         * @return Radius.
         */
        inline double getRadius() const { return radius; }

        /**
         * @brief Gets the base radius.
         * 
         * @return Base radius.
         */
        inline double getBaseRadius() const { return baseRadius; }

        /**
         * @brief Sets the base radius.
         * 
         * @param _baseRadius Base radius.
         */
        inline void setBaseRadius(double _baseRadius) { baseRadius = _baseRadius; }

        /**
         * @brief Increases the radius so that the volume is multiplied by 1 + delta.
         * 
         * @param delta Volume augmentation ratio.
         */
        inline void growth(double delta) {
            this->setRadius(baseRadius * std::cbrt(1 + delta));
        }

        /**
         * @brief Sets the density.
         *
         * Changes the mass knowing the current radius.
         *
         * @param d Density.
         */
        inline void setDensity(double d) {
            density = d;
            this->setMass(density * volume); // Mass = density * 4π/3 * radius³ ng
        }

        /**
         * @brief Gets the adhesion coefficient.
         * @return Adhesion coefficient.
         */
        inline double getAdhesion() const { return adhesion; }

        /**
         * @brief Sets the target overlap.
         * @param overlap Target overlap.
         */
        inline void setTargetOverlap(double _targetOverlap) {
            this->targetOverlap = _targetOverlap;
            computeAdhesion();
        }

        /**
         * @brief Gets the Young's modulus.
         * @return Young's modulus.
         */
        inline double getYoungModulus() const { return youngModulus; }

        /**
         * @brief Sets the Young's modulus.
         * @param ym Young's modulus.
         */
        inline void setYoungModulus(double ym) { youngModulus = ym; }

        /**
         * @brief Gets the purpose velocity.
         * @return Purpose velocity.
         */
        inline MecaCell::Vec getPurposeVelocity() const { return purposeVelocity; }

        /**
         * @brief Sets the purpose velocity.
         * @param pf Purpose velocity.
         */
        inline void setPurposeVelocity(MecaCell::Vec pf) { purposeVelocity = pf; }

        /**
         * @brief Gets the Poisson's ratio.
         * @return Poisson's ratio.
         */
        inline double getPoissonNumber() const { return poissonNumber; }

        /**
         * @brief Sets the Poisson's ratio.
         * @param pn Poisson's ratio.
         */
        inline void setPoissonNumber(double pn) { poissonNumber = pn; }

        /**
         * @brief Moves a cell to position v.
         * @param v Position vector.
         */
        inline void moveTo(const MecaCell::Vec &v) { this->setPosition(v); }

        /**
         * @brief Gets the computed flag.
         * @return Computed flag.
         */
        inline bool getComputed() const { return computed; }

        /**
         * @brief Sets the computed flag.
         * @param c Computed flag.
         */
        inline void setComputed(bool c) { computed = c; }

        /**
         * @brief Gets the gamma value.
         * @return Gamma value.
         */
        inline double getGamma() const { return this->getMass() * gammaCoef; }

        /**
         * @brief Sets the gamma coefficient.
         * @param g Gamma coefficient.
         */
        inline void setGammaCoef(double g) { gammaCoef = g; }

        /**
         * @brief Gets the stress tensor.
         * @return Stress tensor.
         */
        inline Tensor getStressTensor() { return stressTensor; }

        /**
         * @brief Sets the stress tensor.
         * @param st Stress tensor.
         */
        inline void setStressTensor(Tensor st) { stressTensor = st; }

        /**
         * @brief Adds stress to the stress tensor.
         * 
         * @param st Stress tensor.
         */
        inline void addStress(Tensor st) { stressTensor += st; }

        /**
         * @brief Divides the stress tensor.
         * 
         * @param div Divisor.
         */
        inline void divTensor(double div) { stressTensor /= div; }

        /**
         * @brief Gets the internal pressure.
         *
         * Computes the internal pressure based on the stress tensor in ng/(µm.s²).
         * 
         * @return Internal pressure.
         */
        inline double getPressure() const { return abs((stressTensor.get(0, 0) + stressTensor.get(1, 1) + stressTensor.get(2, 2)) / 3.0); }
    };
}

#endif // BODYMASSSPRINGDAMPER_HPP
