#ifndef TENSOR
#define TENSOR

#include <mecacell/mecacell.h>

/**
 * @struct Tensor
 * @brief Represents a 3x3 tensor for stress calculations.
 */
struct Tensor {
private:
    double matrix[3][3] = {{0.,0.,0.},{0.,0.,0.},{0.,0.,0.}}; /**< 3x3 matrix representing the tensor */

public:
    /**
     * @brief Default constructor.
     */
    Tensor() {}

    /**
     * @brief Constructor with force and position vectors.
     * 
     * @param f Force vector.
     * @param r Position vector.
     */
    Tensor(const MecaCell::Vec &f, const MecaCell::Vec &r){
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < 3; ++j){
                matrix[i][j] = f.coords[i] * r.coords[j];
            }
        }
    }


    /**
     * @brief Gets the value at the specified position in the tensor.
     * 
     * @param i Row index.
     * @param j Column index.
     * @return Value at the specified position.
     */
    inline double get(int i, int j) const { return matrix[i][j]; }




    /**
     * @brief Divides the tensor by a scalar value.
     * 
     * @param div Scalar value to divide by.
     * @return Reference to the updated tensor.
     */
    Tensor& operator/=(double div) {
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j <3; ++j){
                matrix[i][j] /= div;
            }
        }
        return(*this);
    }


    /**
     * @brief Adds another tensor to this tensor.
     * 
     * @param t Tensor to add.
     * @return Reference to the updated tensor.
     */
    Tensor& operator+=(Tensor t){
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j <3; ++j){
                matrix[i][j] += t.get(i,j);
            }
        }
        return(*this);
    }


    /**
     * @brief Converts the tensor to a string representation.
     * 
     * @return String representation of the tensor.
     */
    string to_string(){
        ostringstream str;
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j <3; ++j){
               str << matrix[i][j] << ", ";
            }
            str << "\n";
        }
        return str.str();
    }

};

#endif // TENSOR